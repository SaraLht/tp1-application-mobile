package com.example.hp.ceriapplication1;

import android.content.Context;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import java.util.Arrays;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    ListView listad;
    ArrayList<String[]> arrayList = new ArrayList<>();
    int[] img = {R.drawable.panda, R.drawable.koala, R.drawable.fox, R.drawable.bear, R.drawable.cow, R.drawable.camel, R.drawable.lion};
    ArrayList animalNames = new ArrayList<>(Arrays.asList(AnimalList.getNameArray()));
    ArrayList animalImages = new ArrayList<>(Arrays.asList(R.drawable.panda, R.drawable.koala, R.drawable.fox, R.drawable.bear, R.drawable.cow, R.drawable.camel, R.drawable.lion));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // get the reference of RecyclerView
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        // set a LinearLayoutManager with default horizontal orientation and false value for reverseLayout to show the items from start to end
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        //  call the constructor of CustomAdapter to send the reference and data to Adapter
        CustomAdapter customAdapter = new CustomAdapter(MainActivity.this, animalNames, animalImages);
        recyclerView.setAdapter(customAdapter); // set the Adapter to RecyclerView


    }

    class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

        ArrayList<String> animalNames;
        ArrayList<Integer> animalImages;
        Context context;

        public CustomAdapter(Context context, ArrayList<String> animalNames, ArrayList<Integer> animalImages) {
            this.context = context;
            this.animalNames = animalNames;
            this.animalImages = animalImages;
        }

        @Override
        public CustomAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // infalte the item Layout
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list, parent, false);
            // set the view's size, margins, paddings and layout parameters
            CustomAdapter.MyViewHolder vh = new CustomAdapter.MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(CustomAdapter.MyViewHolder holder, final int position) {
            // set the data in items
            holder.name.setText(animalNames.get(position));
            holder.image.setImageResource(animalImages.get(position));
            // implement setOnClickListener event on item view.
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // display a toast with person name on item click
                    Toast.makeText(context, animalNames.get(position), Toast.LENGTH_SHORT).show();
                    String animal = animalNames.get(position).toString();
                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    intent.putExtra("animal",animal);
                    startActivity(intent);

                }
            });
        }


        @Override
        public int getItemCount() {
            return animalNames.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            // init the item view's
            TextView name;
            ImageView image;

            public MyViewHolder(View itemView) {
                super(itemView);

                // get the reference of item view's
                name = (TextView) itemView.findViewById(R.id.textView);
                image = (ImageView) itemView.findViewById(R.id.imageView);

            }
        }
    }
}




