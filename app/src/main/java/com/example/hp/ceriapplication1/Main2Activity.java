package com.example.hp.ceriapplication1;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.HashMap;

public class Main2Activity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_main2);
         Intent intent= getIntent();
         final String nomAnimal=intent.getStringExtra("animal");
         final Animal animal= AnimalList.getAnimal(nomAnimal);



        int hightestLifespan= animal.getHightestLifespan();
        int gestationPeriod = animal.getGestationPeriod();
        int adultWeight= animal.getAdultWeight();
        float birthWeight = animal.getBirthWeight();
        final String conservationStatus = animal.getConservationStatus();


        TextView hightLife;
        hightLife  = (TextView) findViewById(R.id.hight);
        hightLife.setText(""+hightestLifespan+"  Années");
        TextView gestation;
        gestation  = (TextView) findViewById(R.id.gestation);
        gestation.setText(""+gestationPeriod+"  Jours");
        final TextView name;
        name  = (TextView) findViewById(R.id.animal);
        name.setText(nomAnimal);
        TextView adult;
        adult  =  (TextView) findViewById(R.id.adult);
        adult.setText(""+adultWeight+"  Kg");
        TextView birth;
        birth  = (TextView) findViewById(R.id.birth);
        birth.setText(""+birthWeight+"  Kg");

        final TextView conservationStat;
        conservationStat = (TextView) findViewById(R.id.conservation);
        conservationStat.setText("" + conservationStatus);



        ImageView img;
        int id;
        id= getResources().getIdentifier(animal.getImgFile(),"drawable",getPackageName());
        img= (ImageView) findViewById(R.id.ImgFile);
        img.setImageResource(id);

        Button sauvegarder = (Button) findViewById(R.id.sauvgarder);
        sauvegarder.setOnClickListener(new View.OnClickListener(){
            public  void onClick(View v){
                Toast.makeText(getApplicationContext(), "sauvegarder", Toast.LENGTH_SHORT).show();
                animal.setConservationStatus(conservationStat.getText().toString());
            }
        });

        Button retour = (Button) findViewById(R.id.retour);
        retour.setOnClickListener(new View.OnClickListener(){
            public  void onClick(View v){
                Main2Activity.this.finish();
            }
        });
        }
    }

